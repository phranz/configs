""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Written in 2022 by Francesco Palumbo <phranz.dev@gmail.com>, Naples (Italy)
"
" To the extent possible under law, the author(s) have dedicated
" all copyright and related and neighboring rights to this software
" to the public domain worldwide.
" This software is distributed without any warranty.
" You should have received a copy of the CC0 Public Domain Dedication
" along with this software.
" If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

syntax on
filetype on
filetype plugin on

hi TabLine ctermbg=Black ctermfg=Gray cterm=None
hi TabLineSel ctermbg=Black ctermfg=Green

set omnifunc=syntaxcomplete#Complete
set modeline
set autoindent

set autoread
au FocusGained,BufEnter,BufWinEnter,CursorHold,CursorMoved * :checktime

set ts=4
set sw=4
set smarttab
set noexpandtab
set nu
"set fo+=r

set backspace=indent,eol,start

set background=dark
set tags=tags;/
set mouse=a
"set ttymouse=xterm2

inoremap <F1> <C-V>

inoremap <S-Right> <esc>:tabn<cr>i
nnoremap <S-Right> :tabn<cr>
inoremap <S-Left> <esc>:tabp<cr>i
nnoremap <S-Left> :tabp<cr>

"copy/paste
vnoremap <C-c> "+ygv"*y
inoremap <C-V> <esc>"+pi
vnoremap <C-V> "+pi

noremap <C-S-up> <C-a>
noremap <C-S-down> <C-X>

nnoremap l 'mz.

inoremap <C-a> <esc>gg^vGG$i
nnoremap <C-a> <esc>gg^vGG$

inoremap [Z <C-x><C-o>
inoremap <C-f> <C-x><C-f>

inoremap d <esc>ddi
nnoremap d dd
vnoremap d d

inoremap c <esc>^y$i
nnoremap c ^y$
vnoremap c y

inoremap v <esc>p
nnoremap v p
vnoremap v p

inoremap x <esc>^d$i
nnoremap x ^d$
vnoremap x d

inoremap [29~ ()<left>
inoremap q ''<left>
inoremap w ""<left>

vnoremap [29~ di()<esc>P
vnoremap q di''<esc>P
vnoremap w di""<esc>P

inoremap <C-s> <esc>:w<cr>
nnoremap <C-s> :w<cr>
inoremap S <esc>:wa<cr>
nnoremap S :wa<cr>
inoremap <C-p> <esc>:Lexplore<cr>
nnoremap <C-p> :Lexplore<cr>
inoremap <C-q><C-a> <esc>:qa!<cr>
nnoremap <C-q><C-a> :qa!<cr>
inoremap <C-q><C-q> <esc>:q!<cr>
nnoremap <C-q><C-q> :q!<cr>
inoremap <C-x> <esc>:q<cr>
nnoremap <C-x> :q<cr>

inoremap <C-a>f <esc>:bp<cr>
nnoremap <C-a>f :bp<cr>

inoremap <C-d>" <esc>:sp<cr>
nnoremap <C-d>" :sp<cr>
inoremap <C-d>% <esc>:vsp<cr>
nnoremap <C-d>% :vsp<cr>

nnoremap <S-R> :source ~/.vimrc<cr>

inoremap <M-pageup> <esc>:above new<cr>
nnoremap <M-pageup> :above new<cr>
inoremap <M-pagedown> <esc>:below new<cr>
nnoremap <M-pagedown> :below new<cr>
inoremap <M-home> <esc>:leftabove vnew<cr>
nnoremap <M-home> :leftabove vnew<cr>
inoremap <M-end> <esc>:rightbelow vnew<cr>
nnoremap <M-end> :rightbelow vnew<cr>

inoremap <M-up> <esc>:resize +2<CR>
nnoremap <M-up> :resize +2<CR>
tnoremap <M-up> <C-w>+
inoremap <M-down> <esc>:resize -2<CR>
nnoremap <M-down> :resize -2<CR>
tnoremap <M-down> <C-w>-
inoremap <M-left> <esc>:vertical resize +2<CR>
nnoremap <M-left> :vertical resize +2<CR>
tnoremap <M-left> <C-w><
tnoremap <M-left> <C-w><
inoremap <M-right> <esc>:vertical resize -2<CR>
nnoremap <M-right> :vertical resize -2<CR>
tnoremap <M-right> <C-w>>

inoremap <S-Up> <esc>1yy<up>pi
nnoremap <S-Up> 1yy<up>p
inoremap <S-Down> <esc>1yypi
nnoremap <S-Down> 1yyp

inoremap <C-up> <esc><up>$a<cr>
nnoremap <C-up> <esc><up>$a<cr><esc>
inoremap <C-down> <esc>$a<cr>
nnoremap <C-down> <esc>$a<cr><esc>

function PutLeftVisual()
    normal gvD
    let ln = line(".")
    execute "normal! \<C-w>\<left>"
    execute ln
    normal P
endfunction

function PutRightVisual()
    normal gvD
    let ln = line(".")
    execute "normal! \<C-w>\<right>"
    execute ln
    normal P
endfunction

function PutLeft()
    normal dd
    let ln = line(".")
    execute "normal! \<C-w>\<left>"
    execute ln
    normal P
endfunction

function PutRight()
    normal dd
    let ln = line(".")
    execute "normal! \<C-w>\<right>"
    execute ln
    normal P
endfunction

function CopyToLeftVisual()
    normal gvy
    let ln = line(".")
    execute "normal! \<C-w>\<left>"
    execute ln
    normal P
endfunction

function CopyToRightVisual()
    normal gvy
    let ln = line(".")
    execute "normal! \<C-w>\<right>"
    execute ln
    normal P
endfunction

function CopyToLeft()
    normal 1yy
    let ln = line(".")
    execute "normal! \<C-w>\<left>"
    execute ln
    normal P
endfunction

function CopyToRight()
    normal 1yy
    let ln = line(".")
    execute "normal! \<C-w>\<right>"
    execute ln
    normal P
endfunction

vnoremap <C-left> <esc>:call PutLeftVisual()<cr>
vnoremap <C-right> <esc>:call PutRightVisual()<cr>
inoremap <C-left> <esc>:call PutLeftVisual()<cr>
inoremap <C-right> <esc>:call PutRightVisual()<cr>
nnoremap <C-left> <esc>:call PutLeft()<cr>
nnoremap <C-right> <esc>:call PutRight()<cr>

vnoremap <C-S-left> <esc>:call CopyToLeftVisual()<cr>
vnoremap <C-S-right> <esc>:call CopyToRightVisual()<cr>
inoremap <C-S-left> <esc>:call CopyToLeftVisual()<cr>
inoremap <C-S-right> <esc>:call CopyToRightVisual()<cr>
nnoremap <C-S-left> <esc>:call CopyToLeft()<cr>
nnoremap <C-S-right> <esc>:call CopyToRight()<cr>

augroup autocmds_omnifunc
    autocmd!
    autocmd CompleteDone * pc
augroup END

function CommentDecomment()
    let f = &filetype
    let c = "#"
    if f == "sh" || f == "ruby" || f == "python"
        let c = "#"
    elseif f == "c" || f == "cpp" || f == "go"
        let c = "//"
    elseif f == "vim"
        let c = "\""
    elseif f == "haskell"
        let c = "--"
    else
        let c = "#"
    endif
    if getline('.') =~ '^'.c
        execute 's;^'.c.';;e'
    else
        execute 's;^\([^'.c.']\);'.c.'\1;e'
    endif
endfunction

nnoremap <S-c> :call CommentDecomment()<cr>
vnoremap <S-c> :call CommentDecomment()<cr>

execute "set <M-cr>=\<esc>\<cr>"
inoremap <M-cr> <esc>:term<cr>
nnoremap <M-cr> :term<cr>

tnoremap <C-x> exit<cr>

let g:sn = -1

function RunScript()
    let f = &filetype
    let dir = expand('%:p:h')
    let prg = expand('%:p')

    if f == "sh" || f == "ruby" || f == "python"
        let cmd = "cd ".dir." && ".f." ".prg." 2<&1"
    elseif f == "go"
        let cmd = "cd ".dir." && go run ".prg." 2<&1"
    elseif f == "c"
        let cmd = "cd ".dir." && cc -ggdb -O0 ".prg." 2<&1 && ./a.out && unlink a.out 2>/dev/null"
    elseif f == "cpp" || f == "cc"
        let cmd = "cd ".dir." && g++ -ggdb -O0 ".prg." 2<&1 && ./a.out && unlink a.out 2>/dev/null"
    elseif f == "haskell"
        let cmd = "cd ".dir." && ghc ".prg." -o a.out 2<&1 && ./a.out && unlink a.out 2>/dev/null"
    else
        let cmd = "cd ".dir." && sh ".prg." 2<&1"
    endif
    if ! bufexists(g:sn)
        sp
        noswapfile hide enew
        setlocal buftype=nofile
        setlocal bufhidden=hide
        let g:sn = bufnr('%')
        execute "r!".cmd
        execute "normal! \<c-w>\<c-p>"
    elseif bufwinnr(g:sn) < 0
        sp
        execute "b ".g:sn
        execute "0,$d"
        execute "r!".cmd
        execute "normal! \<c-w>\<c-p>"
    else
        execute bufwinnr(g:sn) .. "wincmd w"
        execute "0,$d"
        execute "r!".cmd
        execute "normal! \<c-w>\<c-p>"
    endif
endfunction

function EvalSelection()
    let cmd = getreg('+')
    if ! bufexists(g:sn)
        sp
        noswapfile hide enew
        setlocal buftype=nofile
        setlocal bufhidden=hide
        let g:sn = bufnr('%')
        execute "r!".cmd
        execute "normal! \<c-w>\<c-p>"
    elseif bufwinnr(g:sn) < 0
        sp
        execute "b ".g:sn
        execute "0,$d"
        execute "r!".cmd
        execute "normal! \<c-w>\<c-p>"
    else
        execute bufwinnr(g:sn) .. "wincmd w"
        execute "0,$d"
        execute "r!".cmd
        execute "normal! \<c-w>\<c-p>"
    endif
endfunction

function SubSelection()
    let cmd = getreg('+')
    normal gvd
    execute "normal! \<up>"
    execute "r!".cmd
    execute "normal! \<up>"
endfunction

inoremap <F12> <esc>:call RunScript()<cr><cr>i
nnoremap <F12> :call RunScript()<cr><cr>
vnoremap <F12> "+y:call EvalSelection()<cr><cr>
vnoremap <F11> "+y:call SubSelection()<cr><cr>

function GrepSearch()
    let p = input('')
    normal :call input('')
    echo "searching ".p
    execute "vimgrep /".p."/j %:p:h/**"
    call feedkeys('<cr><c-w><down>') 
    copen
endfunction

nnoremap <F2> :call GrepSearch()<cr>
inoremap <F2> <esc>:call GrepSearch()<cr>

function Enclose()
    normal gv
    let enc = input('')
    execute "normal I".enc
    normal gv
    execute "normal A".enc
endfunction

function Between()
    normal gv
    let p1 = input('part 1> ')
    let p2 = input('part 2> ')
    execute "normal I".p1
    normal gv
    execute "normal A".p2
endfunction

vnoremap e <esc>:call Enclose()<cr>
vnoremap b <esc>:call Between()<cr>

let g:cs_loaded = 0

function! LoadCS()
    if (g:cs_loaded == 0)
        let f = &filetype
        if f == "c" || f == "cpp"
            let g:cs_loaded = 1
            let csfile = findfile("cscope.out", ".;")
            if (!empty(csfile))
                let path = strpart(csfile, 0, match(csfile, "/cscope.out$"))
                exe "cs add ".csfile." ".path
            endif
        endif
    endif
endfunction
au BufEnter * call LoadCS()


let g:netrw_banner = 0
let g:netrw_liststyle = 3
let g:netrw_browse_split = 4
let g:netrw_altv = 1
let g:netrw_winsize = 15

let g:gpl_template = "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n
\% Copyright (C) <vimyear>  <vimname> <vimsurname> <vimemail>, <vimcity> <vimnation>\n
\%\n
\% This program is free software: you can redistribute it and/or modify\n
\% it under the terms of the GNU General Public License as published by\n
\% the Free Software Foundation, either version 3 of the License, or\n
\% (at your option) any later version.\n
\%\n
\% This program is distributed in the hope that it will be useful,\n
\% but WITHOUT ANY WARRANTY; without even the implied warranty of\n
\% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n
\% GNU General Public License for more details.\n
\%\n
\% You should have received a copy of the GNU General Public License\n
\% along with this program.  If not, see <https://www.gnu.org/licenses/>.\n
\%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n"

let g:mit_template = "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n
\% Copyright (C) <vimyear>  <vimname> <vimsurname> <vimemail>, <vimcity> <vimnation>\n
\% \n
\% Permission is hereby granted, free of charge, to any person \n
\% obtaining a copy of this software and associated documentation files\n
\% (the \"Software\"), to deal in the Software without restriction, \n
\% including without limitation the rights to use, copy, modify, \n
\% merge, publish, distribute, sublicense, and/or sell copies of the Software, \n
\% and to permit persons to whom the Software is furnished to do so, \n
\% subject to the following conditions:\n
\% \n
\% The above copyright notice and this permission notice \n
\% shall be included in all copies or substantial portions of the Software.\n
\% \n
\% THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, \n
\% EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES \n
\% OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. \n
\% IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, \n
\% DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, \n
\% ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE \n
\% OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.\n
\%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n"

let g:cc0_template = "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n
\% Written in <vimyear> by <vimname> <vimsurname> <vimemail>, <vimcity> <vimnation>\n
\%\n
\% To the extent possible under law, the author(s) have dedicated \n
\% all copyright and related and neighboring rights to this software \n
\% to the public domain worldwide. \n
\% This software is distributed without any warranty.\n
\% You should have received a copy of the CC0 Public Domain Dedication \n
\% along with this software. \n
\% If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.\n
\%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n"

let g:vimyear = systemlist('date +%Y')[0]
let g:vimname = 'Francesco'
let g:vimsurname = 'Palumbo'
let g:vimemail = '<phranz.dev@gmail.com>'
let g:vimcity = 'Naples'
let g:vimnation = 'Italy'

function AddLicense(l)
    let f = &filetype
    let ltext = g:gpl_template
    if a:l ==? "CC" || a:l ==? "CC0"
        let ltext = g:cc0_template
    elseif a:l ==? "MIT"
        let ltext = g:mit_template
    elseif a:l ==? "GPL" || a:l ==? "GPL3"
        let ltext = g:gpl_template
    else
        return
    endif
    let ltext = substitute(ltext, '<vimyear>', g:vimyear, '')
    let ltext = substitute(ltext, '<vimname>', g:vimname, '')
    let ltext = substitute(ltext, '<vimsurname>', g:vimsurname, '')
    let ltext = substitute(ltext, '<vimemail>', g:vimemail, '')
    let ltext = substitute(ltext, '<vimcity>', g:vimcity, '')
    let ltext = substitute(ltext, '<vimnation>', g:vimnation, '')
    if f == 'c' || f == 'cpp' || f == 'go'
        let ltext = substitute(ltext, "%", '*', 'g')
        let ltext = substitute(ltext, "^", '/*', 'g')
        let ltext = substitute(ltext, ".$", '*/', 'g')
    elseif f == 'sh' || f == 'python' || f == 'ruby'
        let ltext = substitute(ltext, "%", '#', 'g')
    elseif f == 'vim'
        let ltext = substitute(ltext, "%", '"', 'g')
    elseif f == 'haskell'
        let ltext = substitute(ltext, "%", '--', 'g')
    else
        let ltext = substitute(ltext, "%", '#', 'g')
    endif
    0put =ltext
endfunction

command -nargs=1 License :call AddLicense(<f-args>)
