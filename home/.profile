# ~/.profile: executed by the command interpreter for login shells.
# This file is not read by bash(1), if ~/.bash_profile or ~/.bash_login
# exists.
# see /usr/share/doc/bash/examples/startup-files for examples.
# the files are located in the bash-doc package.

# the default umask is set in /etc/profile; for setting the umask
# for ssh logins, install and configure the libpam-umask package.
#umask 022

if test -z "${XDG_RUNTIME_DIR}"; then
  export XDG_RUNTIME_DIR=/tmp/$(id -u)-runtime-dir
  if ! test -d "${XDG_RUNTIME_DIR}"; then
    mkdir "${XDG_RUNTIME_DIR}"
    chmod 0700 "${XDG_RUNTIME_DIR}"
  fi
fi

HISTSIZE=1000
HISTFILESIZE=2000

export PS1="\[\e[0;36m\]\u\[\e[0m\]\[\e[2;33m\]@\[\e[0m\]\[\e[0;32m\]\h\[\e[0m\] _-^-_-^-_-^- \w # "
export PATH="$PATH:$HOME/bin:$HOME/.local/bin:$HOME/.local/share/gem/ruby/3.2.0/bin:$HOME/go/bin:$HOME/.cargo/bin"

command -v fortune >/dev/null && {
    printf '\n'
    fortune -a
    printf '\n'
}
. ~/.shuman/shuman

alias ls='ls --color=auto'
alias ll='ls --color=auto -lka'
alias ..='cd ..'

test "$(tty)" = /dev/tty1 && test ! "$(pgrep X)" && startx
